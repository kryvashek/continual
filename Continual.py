# -*- coding: utf-8 -*-

from abc import abstractmethod
from typing import Union, Callable, Any  # works on 3.5.2
from functools import wraps

Real = Union[int, float]
RealTuple = (int, float)


class IContinuous:
    @abstractmethod
    def __float__(self) -> float: ...

    @abstractmethod
    def __neg__(self) -> 'IContinuous': ...

    @abstractmethod
    def __pos__(self) -> 'IContinuous': ...

    @abstractmethod
    def __abs__(self) -> 'IContinuous': ...

    @abstractmethod
    def __round__(self, n=None) -> 'IContinuous': ...

    @abstractmethod
    def __lt__(self, other) -> bool: ...

    @abstractmethod
    def __le__(self, other) -> bool: ...

    @abstractmethod
    def __gt__(self, other) -> bool: ...

    @abstractmethod
    def __ge__(self, other) -> bool: ...

    @abstractmethod
    def __eq__(self, other) -> bool: ...

    @abstractmethod
    def __ne__(self, other) -> bool: ...


class Continuous(IContinuous):
    @abstractmethod
    def __float__(self) -> float: ...

    def __neg__(self) -> 'Continuous':
        return self.__class__(-float(self))

    def __pos__(self) -> 'Continuous':
        return self.__class__(float(self))

    def __abs__(self) -> 'Continuous':
        return self.__class__(abs(float(self)))

    def __round__(self, n=None) -> 'Continuous':
        return self.__class__(round(float(self), n))

    def __lt__(self, other) -> bool:
        return float(self) < float(other)

    def __le__(self, other) -> bool:
        return float(self) <= float(other)

    def __gt__(self, other) -> bool:
        return float(self) > float(other)

    def __ge__(self, other) -> bool:
        return float(self) >= float(other)

    def __eq__(self, other) -> bool:
        return float(self) == float(other)

    def __ne__(self, other) -> bool:
        return float(self) != float(other)


class IExtContinuous(IContinuous):
    ComparisonMethod = Callable[['IExtContinuous', float], bool]
    DecoratedComparisonMethod = Callable[['IExtContinuous', Any], bool]

    @staticmethod
    def _comparison(method: ComparisonMethod) -> DecoratedComparisonMethod:
        @wraps(method)
        def wrapped_method(self, other):
            convop = self.convert(other)
            return NotImplemented if convop is NotImplemented else method(self, convop)

        return wrapped_method

    @classmethod
    @abstractmethod
    def convert(cls, other) -> float: ...


class ExtContinuous(Continuous, IExtContinuous):
    @classmethod
    @abstractmethod
    def convert(cls, other) -> float: ...

    @abstractmethod
    def __float__(self) -> float: ...

    @IExtContinuous._comparison
    def __lt__(self, other) -> bool:
        return float(self) < other

    @IExtContinuous._comparison
    def __le__(self, other) -> bool:
        return float(self) <= other

    @IExtContinuous._comparison
    def __gt__(self, other) -> bool:
        return float(self) > other

    @IExtContinuous._comparison
    def __ge__(self, other) -> bool:
        return float(self) >= other

    @IExtContinuous._comparison
    def __eq__(self, other) -> bool:
        return float(self) == other

    @IExtContinuous._comparison
    def __ne__(self, other) -> bool:
        return float(self) != other
