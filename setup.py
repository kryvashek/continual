#!/usr/bin/env python3

# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from json import load

if __name__ == '__main__':
    about = load(open('package.json'))
    namever = '%s %s' % (about['name'], about['version'])
    print('Starting %s setup' % namever )
    setup(name=about['name'], version=about['version'], packages=find_packages())
    print('Finished %s setup' % namever )
